#include <check.h>
#include <stdlib.h>
#include <string.h>

#include "rlp.h"


START_TEST(rlp_decode_single_test) {
	char s = 42;
	char buf;
	char *zbuf = (char*)&buf;
	int r;
	int l;

	char state = RLP_STRING;

	rlp_encoder_t encoder;
	
	rlp_init(&encoder, 1, &s);
	r = rlp_next(&encoder, &l, &zbuf);
	ck_assert_mem_eq(&encoder.state, &state, 1);
	ck_assert_int_eq(r, 0);
	ck_assert_int_eq(l, 1);
	ck_assert_mem_eq(zbuf, &s, l);

	rlp_free(&encoder);
		
}
END_TEST

START_TEST(rlp_decode_short_string_test) {
	int r;
	char s[5] = {0x83, 0x66, 0x6f, 0x6f};
	char target[] = "foo";

	int l;
	char buf[1024];
	char *zbuf = (char*)buf;

	char state = RLP_STRING;

	rlp_encoder_t encoder;
	
	rlp_init(&encoder, 5, s);
	r = rlp_next(&encoder, &l, &zbuf);
	ck_assert_mem_eq(&encoder.state, &state, 1);
	ck_assert_int_eq(r, 0);
	ck_assert_int_eq(l, 3);
	ck_assert_mem_eq(zbuf, target, l);

	rlp_free(&encoder);

}
END_TEST


START_TEST(rlp_decode_string_trailing_zeros_test) {
	int r;
	char s[5] = {0x84, 0x66, 0x6f, 0x6f, 0x00};
	char target[] = "foo\0";

	int l;
	char buf[1024];
	char *zbuf = (char*)buf;

	char state = RLP_STRING;

	rlp_encoder_t encoder;
	
	rlp_init(&encoder, 5, s);
	r = rlp_next(&encoder, &l, &zbuf);
	ck_assert_mem_eq(&encoder.state, &state, 1);
	ck_assert_int_eq(r, 0);
	ck_assert_int_eq(l, 4);
	ck_assert_mem_eq(zbuf, target, l);

	rlp_free(&encoder);

}
END_TEST

START_TEST(rlp_decode_long_string_test) {
	int l;
	int r;

	char s[58];
	s[0] = 0xb8;
	s[1] = 56;

	char buf[1024];
	char *zbuf = (char*)buf;

	char state = RLP_STRING;

	rlp_encoder_t encoder;
	
	rlp_init(&encoder, 57, s);

	r = rlp_next(&encoder, &l, &zbuf);
	ck_assert_mem_eq(&encoder.state, &state, 1);
	ck_assert_int_eq(l, 56);
	ck_assert_int_eq(r, 0);
	ck_assert_mem_eq(zbuf, s+2, l);

	rlp_free(&encoder);

}
END_TEST


START_TEST(rlp_decode_zero_list_test) {
	int l;
	int r;

	char s = 0xc0;
	char buf = 0;
	char *zbuf = (char*)&buf;

	char state = RLP_LIST_DESCEND;

	rlp_encoder_t encoder;
	
	rlp_init(&encoder, 1, &s);
	r = rlp_next(&encoder, &l, &zbuf);
	ck_assert_mem_eq(&encoder.state, &state, 1);
	ck_assert_int_eq(l, 0);
	ck_assert_int_eq(r, 0);

	rlp_free(&encoder);
}
END_TEST


START_TEST(rlp_decode_short_list_test) {
	int l;
	int r;

	char s[2] = {0xc1, 0x80};
	char buf[2];
	char *zbuf = (char*)&buf;

	char state = RLP_LIST_DESCEND;

	rlp_encoder_t encoder;
	
	rlp_init(&encoder, 2, s);
	r = rlp_next(&encoder, &l, &zbuf);
	ck_assert_mem_eq(&encoder.state, &state, 1);
	ck_assert_int_eq(l, 1);
	ck_assert_int_eq(r, 0);

	rlp_free(&encoder);
}
END_TEST


START_TEST(rlp_decode_long_list_test) {
	int l;
	int r;

	char s[58];
	s[0] = 0xf8;
	s[1] = 56;
	s[2] = 0x80 + 55;

	char buf[56];
	char *zbuf = (char*)&buf;

	char state = RLP_LIST_DESCEND;

	rlp_encoder_t encoder;
	
	rlp_init(&encoder, 58, s);
	r = rlp_next(&encoder, &l, &zbuf);
	ck_assert_mem_eq(&encoder.state, &state, 1);
	ck_assert_int_eq(l, 56);
	ck_assert_int_eq(r, 0);

	rlp_free(&encoder);
}
END_TEST


START_TEST(rlp_decode_adjacent_strings_test) {
	int l;
	int r;

	char s[] = {0x83, 0x66, 0x6f, 0x6f, 0x84, 0x62, 0x61, 0x72, 0x00};

	char state = RLP_STRING;

	rlp_encoder_t encoder;
	
	char buf[4];
	char *zbuf = (char*)&buf;

	rlp_init(&encoder, 9, s);
	r = rlp_next(&encoder, &l, &zbuf);
	ck_assert_mem_eq(&encoder.state, &state, 1);
	ck_assert_int_eq(r, 0);
	ck_assert_int_eq(l, 3);
	ck_assert_mem_eq(zbuf, "foo", 3);

	r = rlp_next(&encoder, &l, &zbuf);
	ck_assert_mem_eq(&encoder.state, &state, 1); // fail because every string process decrements depth, should only at end of list
	ck_assert_int_eq(r, 0);
	ck_assert_int_eq(l, 4);
	ck_assert_mem_eq(zbuf, "bar\0", 4);
}
END_TEST

Suite *rlp_decode_suite(void) {
	Suite *s;
	TCase *tcb;
	TCase *tcl;

	s = suite_create("rlp_decode");
	tcb = tcase_create("bytes"); 
	tcl = tcase_create("list"); 

	tcase_add_test(tcb, rlp_decode_single_test);
	tcase_add_test(tcb, rlp_decode_short_string_test);
	tcase_add_test(tcb, rlp_decode_string_trailing_zeros_test);
	tcase_add_test(tcb, rlp_decode_long_string_test);
	suite_add_tcase(s, tcb);

	tcase_add_test(tcl, rlp_decode_zero_list_test);
	tcase_add_test(tcl, rlp_decode_short_list_test);
	tcase_add_test(tcl, rlp_decode_long_list_test);
	tcase_add_test(tcl, rlp_decode_adjacent_strings_test);
	suite_add_tcase(s, tcl);

	return s;
}


int main(void) {
	int num_fail;

	Suite *s;
	SRunner *sr;

	s = rlp_decode_suite();
	sr = srunner_create(s);

	srunner_run_all(sr, CK_VERBOSE);
	num_fail = srunner_ntests_failed(sr);
	srunner_free(sr);

	return (num_fail == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
