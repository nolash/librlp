#include <check.h>

#include "rlp.h"


START_TEST(rlp_init_alloc_test) {
	rlp_encoder_t encoder;

	rlp_init(&encoder, 1024, NULL);

	ck_assert_int_eq(encoder.state, RLP_ENCODE);

	rlp_free(&encoder);
}
END_TEST


START_TEST(rlp_init_noalloc_test) {
	char something[] = "foo";

	rlp_encoder_t encoder;

	rlp_init(&encoder, 4, something);

	ck_assert_int_eq(encoder.state, RLP_DECODE);

	rlp_free(&encoder);
}
END_TEST

Suite *rlp_init_suite(void) {
	Suite *s;
	TCase *tc;

	s = suite_create("rlp");
	tc = tcase_create("init");

	tcase_add_test(tc, rlp_init_alloc_test);
	suite_add_tcase(s, tc);

	return s;
}


int main(void) {
	int num_fail;

	Suite *s;
	SRunner *sr;

	s = rlp_init_suite();
	sr = srunner_create(s);

	srunner_run_all(sr, CK_VERBOSE);
	num_fail = srunner_ntests_failed(sr);
	srunner_free(sr);

	return (num_fail == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
