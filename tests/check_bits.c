#include <check.h>
#include <stdlib.h>
#include <string.h>

#include "bits.h"

START_TEST(le_test) {
	char v[10];
	int r;

	memset(v, 0, 10);

	r = intbits_le(0, NULL);
	ck_assert_int_eq(r, 0);

	r = intbits_le(10, (char*)v);
	ck_assert_int_eq(r, 1);

	v[0] = 2;
	r = intbits_le(10, (char*)v);
	ck_assert_int_eq(r, 2);

	v[1] = 2;
	r = intbits_le(10, v);
	ck_assert_int_eq(r, 10);
	r = intbytes_le(10, v);
	ck_assert_int_eq(r, 2);

	v[9] = 7;
	r = intbits_le(10, v);
	ck_assert_int_eq(r, 75);
	r = intbytes_le(10, v);
	ck_assert_int_eq(r, 10);
}
END_TEST


START_TEST(le_boundary_test) {
	char v[10];
	int r;

	memset(v, 0, 10);


	v[1] = 0x80;
	r = intbytes_le(10, v);
	ck_assert_int_eq(r, 2);

	v[2] = 0x01;
	r = intbytes_le(10, v);
	ck_assert_int_eq(r, 3);

}
END_TEST


Suite *bits_suite(void) {
	Suite *s;
	TCase *tc;

	s = suite_create("Rlp");
	tc = tcase_create("Init");

	tcase_add_test(tc, le_test);
	tcase_add_test(tc, le_boundary_test);
	suite_add_tcase(s, tc);

	return s;
}


int main(void) {
	int num_fail;

	Suite *s;
	SRunner *sr;

	s = bits_suite();
	sr = srunner_create(s);

	srunner_run_all(sr, CK_VERBOSE);
	num_fail = srunner_ntests_failed(sr);
	srunner_free(sr);

	return (num_fail == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
