#include <check.h>
#include <stdlib.h>
#include <string.h>

#include "rlp.h"

START_TEST(rlp_dog_test) {
	char buf[1024];
	char *zbuf = (char*)&buf;
	int l;

	char state = RLP_STRING;

	rlp_encoder_t encoder;

	rlp_init(&encoder, 1024, NULL);
	char *x_dog = "dog";
	char r_dog[4] = {0x83, 'd', 'o', 'g'};
	rlp_add(&encoder, 3, x_dog);	
	ck_assert_mem_eq(encoder.buf, r_dog, 4);
	ck_assert_int_eq(encoder.size, 4);

	// reuse for decode
	rlp_init(&encoder, 4, encoder.buf);
	rlp_next(&encoder, &l, &zbuf);
	ck_assert_mem_eq(&encoder.state, &state, 1);
	ck_assert_int_eq(l, 3);
	ck_assert_mem_eq(zbuf, x_dog, l);

	state = RLP_END;
	rlp_next(&encoder, &l, &zbuf);
	ck_assert_mem_eq(&encoder.state, &state, 1);

	rlp_free(&encoder);
}
END_TEST

START_TEST(rlp_catdog_test) {
	rlp_encoder_t encoder;

	char buf[1024];
	char *zbuf = (char*)&buf;
	int l;
	char state;

	char *x_dog = "dog";
	char *x_cat = "cat";
	char r_catdog[9] = {0xc8, 0x83, 'c', 'a', 't', 0x83, 'd', 'o', 'g'};
	
	rlp_init(&encoder, 1024, NULL);
	rlp_descend(&encoder);
	rlp_add(&encoder, 3, x_cat);	
	rlp_add(&encoder, 3, x_dog);
	rlp_ascend(&encoder);
	ck_assert_mem_eq(encoder.buf, r_catdog, 9);
	ck_assert_int_eq(encoder.size, 9); 

	// reuse for decode
	state = RLP_LIST_DESCEND;
	rlp_init(&encoder, 9, encoder.buf);
	rlp_next(&encoder, &l, &zbuf);
	ck_assert_mem_eq(&encoder.state, &state, 1);
	ck_assert_int_eq(l, 8);

	state = RLP_STRING;
	rlp_next(&encoder, &l, &zbuf);
	ck_assert_mem_eq(&encoder.state, &state, 1);
	ck_assert_mem_eq(zbuf, x_cat, l);

	rlp_next(&encoder, &l, &zbuf);
	ck_assert_mem_eq(&encoder.state, &state, 1);
	ck_assert_mem_eq(zbuf, x_dog, l);

	state = RLP_END;
	rlp_next(&encoder, &l, &zbuf);
	ck_assert_mem_eq(&encoder.state, &state, 1);

	rlp_free(&encoder);
}
END_TEST

START_TEST(rlp_lorem_test) {
	rlp_encoder_t encoder;

	char buf[1024];
	char *zbuf = (char*)&buf;
	int l;
	char state;

	char *lorem = "Lorem ipsum dolor sit amet, consectetur adipisicing elit";
	char target[2] = {0xb8, strlen(lorem)};

	rlp_init(&encoder, 1024, NULL);
	rlp_add(&encoder, strlen(lorem), lorem);
	ck_assert_mem_eq(encoder.buf, target, 2);
	ck_assert_mem_eq(encoder.buf+2, lorem, strlen(lorem));
	ck_assert_int_eq(encoder.size, 2 + strlen(lorem));

	// reuse for decode
	state = RLP_STRING;
	rlp_init(&encoder, strlen(lorem) + 2, encoder.buf);
	rlp_next(&encoder, &l, &zbuf);
	ck_assert_mem_eq(&encoder.state, &state, 1);
	ck_assert_int_eq(l,  strlen(lorem));
	ck_assert_mem_eq(zbuf, lorem, l);

	state = RLP_END;
	rlp_next(&encoder, &l, &zbuf);
	ck_assert_mem_eq(&encoder.state, &state, 1);

	rlp_free(&encoder);
}
END_TEST

START_TEST(rlp_set_theoretical_representation_of_three) {

	unsigned char target[8] = {0xc7, 0xc0, 0xc1, 0xc0, 0xc3, 0xc0, 0xc1, 0xc0};
	int target_lengths[15] = {
		7,
		0,
		0,
		1,
		0,
		0,
		0,
		3,
		0,
		0,
		1,
		0,
		0,
		0,
		0,
	};

	enum rlp_state target_states[15] = {
		RLP_LIST_DESCEND,
		RLP_LIST_DESCEND,
		RLP_LIST_ASCEND,
		RLP_LIST_DESCEND,
		RLP_LIST_DESCEND,
		RLP_LIST_ASCEND,
		RLP_LIST_ASCEND,
		RLP_LIST_DESCEND,
		RLP_LIST_DESCEND,
		RLP_LIST_ASCEND,
		RLP_LIST_DESCEND,
		RLP_LIST_DESCEND,
		RLP_LIST_ASCEND,
		RLP_LIST_ASCEND,
		RLP_LIST_ASCEND,
	};

	char *zbuf = NULL;
	int i;
	int l;
	char state = RLP_LIST_ASCEND;

	rlp_encoder_t encoder;

	rlp_init(&encoder, 1024, NULL);

	// [  []  [ [] ]  [ [] [ []] ]  ]
	rlp_descend(&encoder);

	rlp_descend(&encoder);
	rlp_ascend(&encoder);

	rlp_descend(&encoder);
	rlp_descend(&encoder);
	rlp_ascend(&encoder);
	rlp_ascend(&encoder);

	rlp_descend(&encoder);
	rlp_descend(&encoder);
	rlp_ascend(&encoder);
	rlp_descend(&encoder);
	rlp_descend(&encoder);
	rlp_ascend(&encoder);
	rlp_ascend(&encoder);
	rlp_ascend(&encoder);

	rlp_ascend(&encoder);

	ck_assert_mem_eq(encoder.buf, target, 8);
	ck_assert_int_eq(encoder.size, 8);

	rlp_init(&encoder, 1024, encoder.buf);
	for (i = 0; i < 15; i++) {
		rlp_next(&encoder, &l, &zbuf);
		ck_assert_int_eq(target_states[i], encoder.state);
		ck_assert_int_eq(target_lengths[i], l);
	}

	rlp_free(&encoder);
}
END_TEST


Suite *rlp_vector_suite(void) {
	Suite *s;
	TCase *tcb;

	s = suite_create("rlp_vector");
	tcb = tcase_create("basic"); // examples from https://eth.wiki/fundamentals/rlp
	tcase_add_test(tcb, rlp_dog_test);
	tcase_add_test(tcb, rlp_catdog_test);
	tcase_add_test(tcb, rlp_lorem_test);
	tcase_add_test(tcb, rlp_set_theoretical_representation_of_three);
	suite_add_tcase(s, tcb);


	return s;
}


int main(void) {
	int num_fail;

	Suite *s;
	SRunner *sr;

	s = rlp_vector_suite();
	sr = srunner_create(s);

	srunner_run_all(sr, CK_VERBOSE);
	num_fail = srunner_ntests_failed(sr);
	srunner_free(sr);

	return (num_fail == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
