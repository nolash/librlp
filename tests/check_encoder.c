#include <check.h>
#include <stdlib.h>

#include "rlp.h"



START_TEST(rlp_encode_single_zero_byte_test) {
	char v = 0x00;

	rlp_encoder_t encoder;
	rlp_init(&encoder, 1024, NULL);

	rlp_add(&encoder, 1, &v);
	ck_assert_mem_eq(encoder.buf, &v, 1);

	rlp_free(&encoder);
}
END_TEST


START_TEST(rlp_encode_empty_byte_test) {
	char v = 0x80;

	rlp_encoder_t encoder;
	rlp_init(&encoder, 1024, NULL);

	rlp_add(&encoder, 0, NULL);
	ck_assert_mem_eq(encoder.buf, &v, 1);

	rlp_free(&encoder);
}
END_TEST


START_TEST(rlp_encode_single_byte_test) {
	char v = 55;

	rlp_encoder_t encoder;
	rlp_init(&encoder, 1024, NULL);

	rlp_add(&encoder, 1, &v);
	ck_assert_mem_eq(encoder.buf, &v, 1);

	rlp_free(&encoder);
}
END_TEST


START_TEST(rlp_encode_single_byte_with_length_test) {
	char l = 0x81;
	char v = 56;

	rlp_encoder_t encoder;
	rlp_init(&encoder, 1024, NULL);

	rlp_add(&encoder, 1, &v);
	ck_assert_mem_eq(encoder.buf, &l, 1);
	ck_assert_mem_eq(encoder.buf+1, &v, 1);

	rlp_free(&encoder);
}
END_TEST


START_TEST(rlp_encode_short_bytes_with_length_test) {
	char l = 0xb7;
	char v[55];
	v[54] = 42;

	rlp_encoder_t encoder;
	rlp_init(&encoder, 1024, NULL);

	rlp_add(&encoder, 55, (char*)&v);
	ck_assert_mem_eq(encoder.buf, &l, 1);
	ck_assert_mem_eq(encoder.buf+55, (char*)&(v[54]), 1);

	rlp_free(&encoder);
}
END_TEST


START_TEST(rlp_encode_long_bytes_with_length_test) {
	char l = 0xb8;
	char ll = 56;
	char v[56];
	v[55] = 42;

	rlp_encoder_t encoder;
	rlp_init(&encoder, 1024, NULL);

	rlp_add(&encoder, 56, (char*)&v);
	ck_assert_mem_eq(encoder.buf, &l, 1);
	ck_assert_mem_eq(encoder.buf+1, &ll, 1);
	ck_assert_mem_eq(encoder.buf+57, (char*)&(v[55]), 1);

	rlp_free(&encoder);
}
END_TEST


START_TEST(rlp_encode_list_descend_state_test) {
	int r;
	char *ptr_chk;

	rlp_encoder_t encoder;
	rlp_init(&encoder, 1024, NULL);

	r = rlp_descend(&encoder);
	ck_assert_ptr_eq(encoder.ptr, encoder.list_ptr[encoder.depth]);
	ptr_chk = encoder.ptr;
	ck_assert_int_eq(1, r);
	ck_assert_int_eq(encoder.depth, 1);

	r = rlp_descend(&encoder);
	ck_assert_ptr_eq(encoder.ptr, encoder.list_ptr[encoder.depth]);
	ck_assert_ptr_eq(ptr_chk, encoder.list_ptr[encoder.depth-1]);
	ck_assert_int_eq(2, r);
	ck_assert_int_eq(encoder.depth, 2);

	rlp_free(&encoder);
}
END_TEST


START_TEST(rlp_encode_list_roundtrip_state_test) {
	char one = 42;
	char two = 13;
	int r;

	rlp_encoder_t encoder;
	rlp_init(&encoder, 1024, NULL);

	r = rlp_descend(&encoder);
	ck_assert_int_eq(r, 1);
	rlp_add(&encoder, 1, &one);
	rlp_add(&encoder, 1, &two);
	ck_assert_int_eq(encoder.depth, 1);

	r = rlp_ascend(&encoder);
	ck_assert_int_eq(r, 0);
	ck_assert_int_eq(encoder.depth, 0);

	rlp_free(&encoder);
}
END_TEST



START_TEST(rlp_encode_list_single_depth_test) {
	char one = 42;
	char two = 13;
	char target[3] = {0xc2, 42, 13};
	int r;

	rlp_encoder_t encoder;
	rlp_init(&encoder, 1024, NULL);

	r = rlp_descend(&encoder);
	rlp_add(&encoder, 1, &one);
	rlp_add(&encoder, 1, &two);
	r = rlp_ascend(&encoder);
	ck_assert_mem_eq(encoder.buf, target, 3);
	
	rlp_free(&encoder);
}
END_TEST


START_TEST(rlp_encode_list_adjacent_test) {
	char one[2] = {42, 13};
	char two[3] = {0x66, 0x6f, 0x6f};
	char target[8] = {0xc2, 42, 13, 0xc4, 0x83, 0x66, 0x6f, 0x6f};
	int r;

	rlp_encoder_t encoder;
	rlp_init(&encoder, 1024, NULL);

	r = rlp_descend(&encoder);
	rlp_add(&encoder, 1, &one[0]);
	rlp_add(&encoder, 1, &one[1]);
	r = rlp_ascend(&encoder);
	r = rlp_descend(&encoder);
	rlp_add(&encoder, 3, two);
	r = rlp_ascend(&encoder);
	ck_assert_mem_eq(encoder.buf, target, 8);

	rlp_free(&encoder);
}
END_TEST


START_TEST(rlp_encode_list_nested_test) {
	char one[2] = {42, 13};
	char two[3] = {0x66, 0x6f, 0x6f};
	char target[8] = {0xc7, 42, 13, 0xc4, 0x83, 0x66, 0x6f, 0x6f};
	int r;

	rlp_encoder_t encoder;
	rlp_init(&encoder, 1024, NULL);

	r = rlp_descend(&encoder);
	rlp_add(&encoder, 1, &one[0]);
	rlp_add(&encoder, 1, &one[1]);
	r = rlp_descend(&encoder);
	rlp_add(&encoder, 3, two);
	r = rlp_ascend(&encoder);
	r = rlp_ascend(&encoder);
	ck_assert_mem_eq(encoder.buf, target, 8);

	rlp_free(&encoder);
}
END_TEST

START_TEST(rlp_encode_list_long_test) {
	char v[56];
	char target[4] = {0xf8, 58, 0xb8, 56};
	int r;

	rlp_encoder_t encoder;
	rlp_init(&encoder, 1024, NULL);

	r = rlp_descend(&encoder);
	rlp_add(&encoder, 56, (char*)v);
	r = rlp_ascend(&encoder);
	ck_assert_mem_eq(encoder.buf, target, 4);

	rlp_free(&encoder);
}
END_TEST


START_TEST(rlp_encode_list_long_chunked_test) {
	char x[10];
	char y[20];
	char z[30];
	int i;
	int j;
	int r;
	char *zs;

	char target[] = {0xf8, 63};

	rlp_encoder_t encoder;
	rlp_init(&encoder, 1024, NULL);

	r = rlp_descend(&encoder);

	rlp_add(&encoder, 10, x);
	rlp_add(&encoder, 20, y);
	rlp_add(&encoder, 30, z);

	r = rlp_ascend(&encoder);

	ck_assert_mem_eq(encoder.buf, target, 2);

	rlp_free(&encoder);

}
END_TEST


Suite *rlp_encode_suite(void) {
	Suite *s;
	TCase *tci;
	TCase *tcb;
	TCase *tcl;

	s = suite_create("Rlp");
	tcb = tcase_create("Bytes");
	tcl = tcase_create("List");

	tcase_add_test(tcb, rlp_encode_empty_byte_test);
	tcase_add_test(tcb, rlp_encode_single_zero_byte_test);
	tcase_add_test(tcb, rlp_encode_single_byte_test);
	tcase_add_test(tcb, rlp_encode_single_byte_with_length_test);
	tcase_add_test(tcb, rlp_encode_short_bytes_with_length_test);
	tcase_add_test(tcb, rlp_encode_long_bytes_with_length_test);
	suite_add_tcase(s, tcb);

	tcase_add_test(tcl, rlp_encode_list_descend_state_test);
	tcase_add_test(tcl, rlp_encode_list_roundtrip_state_test);
	tcase_add_test(tcl, rlp_encode_list_single_depth_test);
	tcase_add_test(tcl, rlp_encode_list_adjacent_test);
	tcase_add_test(tcl, rlp_encode_list_nested_test);
	tcase_add_test(tcl, rlp_encode_list_long_test);
	tcase_add_test(tcl, rlp_encode_list_long_chunked_test);
	suite_add_tcase(s, tcl);

	return s;
}


int main(void) {
	int num_fail;

	Suite *s;
	SRunner *sr;

	s = rlp_encode_suite();
	sr = srunner_create(s);

	srunner_run_all(sr, CK_VERBOSE);
	num_fail = srunner_ntests_failed(sr);
	srunner_free(sr);

	return (num_fail == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
