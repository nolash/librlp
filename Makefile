build:
	gcc -I./src $(CFLAGS) -c src/rlp.c -o src/rlp.o
	gcc -I./src $(CFLAGS) -c src/encode.c -o src/encode.o
	gcc -I./src $(CFLAGS) -c src/decode.c -o src/decode.o
	gcc -I./src $(CFLAGS) -c src/bits.c -o src/bits.o
	gcc -I./src $(CFLAGS) -c src/endian.c -o src/endian.o

build_lib: build
	gcc -I./src -fPIC -shared -o librlp.so -Wl,-soname,librlp.so.0.0.1 src/rlp.o src/encode.o src/decode.o src/bits.o src/endian.o


build_test: build
	gcc -I./src -g3 tests/check_rlp.c src/rlp.o src/bits.o src/encode.o src/decode.o src/endian.o -o tests/check_rlp -lcheck
	gcc -I./src -g3 tests/check_encoder.c src/rlp.o src/bits.o src/encode.o src/decode.o src/endian.o -o tests/check_encoder -lcheck
	gcc -I./src -g3 tests/check_bits.c src/rlp.o src/bits.o src/encode.o src/decode.o src/endian.o -o tests/check_bits -lcheck
	gcc -I./src -g3 tests/check_vectors.c src/rlp.o src/bits.o src/encode.o src/decode.o src/endian.o -o tests/check_vectors -lcheck
	gcc -I./src -g3 tests/check_decoder.c src/rlp.o src/bits.o src/encode.o src/decode.o src/endian.o -o tests/check_decoder -lcheck

check: build_test
	tests/check_rlp
	tests/check_encoder
	tests/check_decoder
	tests/check_bits
	tests/check_vectors

.PHONY clean:
	rm -vf tests/check_rlp
	rm -vf tests/check_encoder
	rm -vf tests/check_decoder
	rm -vf tests/check_bits
	rm -vf tests/check_vectors
	rm -vf src/*.o
	rm -vf src/lib*.so*

dist: check
	mkdir -vp dist
	tar -zcvf dist/librlp-`cat VERSION`.tar.gz src/*.h src/*.c Makefile CONTRIBUTORS LICENSE VERSION CHANGELOG


.PHONE distclean: clean
	rm -rf dist/
	

.PHONY test: check
